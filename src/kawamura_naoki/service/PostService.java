package kawamura_naoki.service;

import static kawamura_naoki.utils.CloseableUtil.*;
import static kawamura_naoki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import kawamura_naoki.beans.Post;
import kawamura_naoki.beans.UserPost;
import kawamura_naoki.dao.PostDao;
import kawamura_naoki.dao.UserPostDao;

public class PostService {

	public void register(Post post) {

		Connection connection = null;
		try{
			connection = getConnection();

			PostDao postDao = new PostDao();
			postDao.insert(connection, post);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
		} finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List<UserPost> getPost(String beginDate, String endDate, String category) {

		Connection connection = null;
		try{
			connection = getConnection();

			UserPostDao UserPostDao = new UserPostDao();
			List<UserPost> ret = UserPostDao.getUserPosts(connection, LIMIT_NUM, beginDate, endDate, category);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int post_id) {

		Connection connection = null;
		try{
			connection = getConnection();

			PostDao postDao = new PostDao();
			postDao.delete(connection, post_id);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
