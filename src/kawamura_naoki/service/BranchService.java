package kawamura_naoki.service;

import static kawamura_naoki.utils.CloseableUtil.*;
import static kawamura_naoki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import kawamura_naoki.beans.Branch;
import kawamura_naoki.dao.BranchDao;


public class BranchService {

	public List<Branch> toBranchList() {

		Connection connection = null;
		try{
			connection = getConnection();

			BranchDao BranchDao = new BranchDao();
			List<Branch> ret = BranchDao.pullDown(connection);

			commit(connection);

			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
