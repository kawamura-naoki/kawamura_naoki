package kawamura_naoki.service;

import static kawamura_naoki.utils.CloseableUtil.*;
import static kawamura_naoki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import kawamura_naoki.beans.Position;
import kawamura_naoki.dao.PositionDao;

public class PositionService {

	public List<Position> toPositionList() {

		Connection connection = null;
		try{
			connection = getConnection();

			PositionDao PositionDao = new PositionDao();
			List<Position> ret = PositionDao.pullDown(connection);

			commit(connection);

			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
