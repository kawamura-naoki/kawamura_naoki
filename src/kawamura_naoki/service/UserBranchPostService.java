package kawamura_naoki.service;

import static kawamura_naoki.utils.CloseableUtil.*;
import static kawamura_naoki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import kawamura_naoki.beans.UserBranchPosition;
import kawamura_naoki.dao.UserBranchPositionDao;

public class UserBranchPostService {

	public List<UserBranchPosition> getUserList() {

		Connection connection = null;
		try{
			connection = getConnection();

			UserBranchPositionDao userBranchPositionDao = new UserBranchPositionDao();
			List<UserBranchPosition> ret = userBranchPositionDao.getUserList(connection);

			commit(connection);

			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
