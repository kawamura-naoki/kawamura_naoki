package kawamura_naoki.service;

import static kawamura_naoki.utils.CloseableUtil.*;
import static kawamura_naoki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import kawamura_naoki.beans.Comment;
import kawamura_naoki.beans.UserComment;
import kawamura_naoki.dao.CommentDao;
import kawamura_naoki.dao.UserCommentDao;

public class CommentService {

	public void register(Comment comment) {

		Connection connection = null;
		try{
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserComment> getComment() {

		Connection connection = null;
		try{
			connection = getConnection();

			 List<UserComment> ret = UserCommentDao.getUserComment(connection);

			 commit(connection);

			 return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int comment_id) {

		Connection connection = null;
		try{
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.delete(connection, comment_id);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
