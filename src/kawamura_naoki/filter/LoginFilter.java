package kawamura_naoki.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kawamura_naoki.beans.User;


@WebFilter({"/deletecomment","/deletepost","/index.jsp","/login","/logout","/newpost","/newcomment","/management" , "/edit", "/signup"})
public class LoginFilter implements Filter {
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException,ServletException{

		HttpSession session =((HttpServletRequest)request).getSession();
		String ServletPath = ((HttpServletRequest)request).getServletPath();
		if(!ServletPath.equals("/login")){
			if(session == null){
				List<String> messages = new ArrayList<String>();
				messages.add("ログインしてください");
				session.setAttribute("errorMessages", messages);
				((HttpServletResponse)response).sendRedirect("login");
			} else if((User) session.getAttribute("loginUser") == null) {
				List<String> messages = new ArrayList<String>();
				messages.add("ログインしてください");
				session.setAttribute("errorMessages", messages);
				((HttpServletResponse)response).sendRedirect("login");
			} else {
				chain.doFilter(request, response);
			}
		} else {
			chain.doFilter(request, response);
		}
	}
	public void init(FilterConfig config)throws ServletException {}
	public void destroy(){}
}

