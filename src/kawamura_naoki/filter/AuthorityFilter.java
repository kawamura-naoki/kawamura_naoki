package kawamura_naoki.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kawamura_naoki.beans.User;

@WebFilter({"/management" , "/edit", "/signup"})
public class AuthorityFilter implements Filter {
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException,ServletException{

		HttpSession session = ((HttpServletRequest)request).getSession();
		User user = (User) session.getAttribute("loginUser");
		if(user != null){
			int positionId = user.getPosition_id();
			int branchId = user.getBranch_id();
			if(positionId == 1 & branchId == 1){
				chain.doFilter(request, response);
			} else {
				List<String> messages = new ArrayList<String>();
				messages.add("あなたはページにアクセスする権限がありません");
				session.setAttribute("errorMessages", messages);
				((HttpServletResponse)response).sendRedirect("./");
			}
		} else {

			((HttpServletResponse)response).sendRedirect("./");
		}

	}
	public void init(FilterConfig config)throws ServletException {}
	public void destroy(){}
}
