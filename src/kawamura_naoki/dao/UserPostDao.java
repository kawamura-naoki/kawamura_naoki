package kawamura_naoki.dao;

import static kawamura_naoki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import kawamura_naoki.beans.UserPost;
import kawamura_naoki.exception.SQLRuntimeException;

public class UserPostDao {

	public List<UserPost> getUserPosts(Connection connection, int num, String beginDate, String endDate, String category) {

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id as id, ");
			sql.append("posts.title as title, ");
			sql.append("posts.text as text, ");
			sql.append("posts.category as category, ");
			sql.append("posts.user_id as user_id, ");
			sql.append("users.name as name, ");
			sql.append("posts.created_date as created_date ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON posts.user_id = users.id ");
			sql.append(" WHERE posts.created_date BETWEEN ? ");
			sql.append(" AND ?" );
			if(category != null && category.length() != 0){
				sql.append("AND posts.category LIKE ?");
			}
			sql.append(" ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());




			ps.setString(1, beginDate);
			ps.setString(2, endDate);
			if(category != null && category.length() != 0){
				ps.setString(3, "%" + category + "%");
			}



			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toUserPostList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private  List<UserPost> toUserPostList(ResultSet rs)
			throws SQLException {

		List<UserPost> ret = new ArrayList<UserPost>();
		try{
			while(rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserPost post = new UserPost();
				post.setName(name);
				post.setId(id);
				post.setUser_id(userId);
				post.setTitle(title);
				post.setText(text);
				post.setCategory(category);
				post.setCreated_date(createdDate);

				ret.add(post);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
