package kawamura_naoki.dao;

import static kawamura_naoki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import kawamura_naoki.beans.User;
import kawamura_naoki.exception.NoRowsUpdatedRuntimeException;
import kawamura_naoki.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( " );
			sql.append("login_id ,");
			sql.append("name ,");
			sql.append("password ,");
			sql.append("branch_id ,");
			sql.append("position_id ,");
			sql.append("is_stopped ,");
			sql.append("created_date ,");
			sql.append("updated_date ");
			sql.append(") VALUES (");
			sql.append("?");                     // login_id
			sql.append(", ?");                   // name
			sql.append(", ?");                   // password
			sql.append(", ?");                   // branch_id
			sql.append(", ?");                   // position_id
			sql.append(", ?");                   // is_stopped
			sql.append(", CURRENT_TIMESTAMP");   // create_date
			sql.append(", CURRENT_TIMESTAMP");   // update_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getName());
			ps.setString(3, user.getPassword());
			ps.setInt(4, user.getBranch_id());
			ps.setInt(5, user.getPosition_id());
			ps.setInt(6, 0);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String login_id,
			String password) {

		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? AND is_stopped = 0";

			ps = connection.prepareStatement(sql);
			ps.setString(1,login_id);
			ps.setString(2, password);


			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty() == true){
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try{
			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String password = rs.getString("password");
				int branchId = rs.getInt("branch_id");
				int positionId = rs.getInt("position_id");
				int isStopped  = rs.getInt("is_stopped");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				User user = new User();
				user.setId(id);
				user.setLogin_id(loginId);
				user.setName(name);
				user.setPassword(password);
				user.setBranch_id(branchId);
				user.setPosition_id(positionId);
				user.setIs_stopped(isStopped);
				user.setCreated_date(createdDate);
				user.setUpdated_date(updatedDate);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<User> getUserList(Connection connection) {

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT id, ");
			sql.append("login_id, ");
			sql.append("name, ");
			sql.append("branch_id, ");
			sql.append("position_id, ");
			sql.append("is_stopped, ");
			sql.append("created_date ");
			sql.append("FROM users;");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> ret = toUsers(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUsers(ResultSet rs)
			throws SQLException {

		List<User> ret = new ArrayList<User>();
		try{
			while(rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				int branch_id = rs.getInt("branch_id");
				int position_id = rs.getInt("position_id");
				int is_stopped = rs.getInt("is_stopped");
				Timestamp createdDate = rs.getTimestamp("created_date");

				User userList = new User();
				userList.setId(id);
				userList.setLogin_id(login_id);
				userList.setName(name);
				userList.setBranch_id(branch_id);
				userList.setPosition_id(position_id);
				userList.setIs_stopped(is_stopped);
				userList.setCreated_date(createdDate);

				ret.add(userList);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void isStopped(Connection connection, int id) {

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET is_stopped = 1 WHERE id = ");
			sql.append("? ;");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);

			int count = ps.executeUpdate();
			if(count == 0){
				throw new NoRowsUpdatedRuntimeException();

			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void isResurrection(Connection connection, int id){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET is_stopped = 0 WHERE id = ");
			sql.append("? ;");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);

			int count = ps.executeUpdate();
			if(count == 0){
				throw new NoRowsUpdatedRuntimeException();

			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User>userList = toUserList(rs);
			if(userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException (e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			sql.append(", name = ?");
			if(!user.getPassword().isEmpty()){
				sql.append(", password = ?");
			}
			sql.append(", branch_id = ?");
			sql.append(", position_id = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getName());
			if(user.getPassword().isEmpty()){
				ps.setInt(3, user.getBranch_id());
				ps.setInt(4, user.getPosition_id());
				ps.setInt(5, user.getId());
			} else {
				ps.setString(3, user.getPassword());
				ps.setInt(4, user.getBranch_id());
				ps.setInt(5, user.getPosition_id());
				ps.setInt(6, user.getId());
			}

			int count = ps.executeUpdate();
			if(count == 0){
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User confirmationUser(Connection connection, String loginId){

		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE login_id = ?  AND is_stopped = 0";

			ps = connection.prepareStatement(sql);
			ps.setString(1,loginId);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty() == true){
				return null;
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}