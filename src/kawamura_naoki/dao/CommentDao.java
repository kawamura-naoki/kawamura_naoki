package kawamura_naoki.dao;

import static kawamura_naoki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import kawamura_naoki.beans.Comment;
import kawamura_naoki.exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("post_id");
			sql.append(", user_id");
			sql.append(", text");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(" ) VALUES (");
			sql.append(" ?");  //post_id
			sql.append(", ?"); //user_id
			sql.append(", ?"); //text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getPost_id());
            ps.setInt(2, comment.getUser_id());
            ps.setString(3, comment.getText());

            ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection, int comment_id){

		PreparedStatement ps = null;
		try{
			String sql = "DELETE FROM comments WHERE id = ? ;";

			ps = connection.prepareStatement(sql);

			ps.setInt(1,comment_id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);

		}
	}
}
