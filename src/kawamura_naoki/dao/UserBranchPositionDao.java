package kawamura_naoki.dao;

import static kawamura_naoki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import kawamura_naoki.beans.UserBranchPosition;
import kawamura_naoki.exception.SQLRuntimeException;

public class UserBranchPositionDao {

	public List<UserBranchPosition> getUserList(Connection connection) {

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("users.branch_id as branch_id, ");
			sql.append("branches.name as branch_name, ");
			sql.append("users.position_id as position_id, ");
			sql.append("positions.name as position_name, ");
			sql.append("users.is_stopped as is_stopped, ");
			sql.append("users.created_date as created_date, ");
			sql.append("users.updated_date as updated_date ");
			sql.append("FROM users ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position_id = positions.id ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");

			ps = connection.prepareStatement(sql.toString());


			ResultSet rs = ps.executeQuery();
			List<UserBranchPosition> ret = toUsers(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserBranchPosition> toUsers(ResultSet rs)
			throws SQLException {

		List<UserBranchPosition> ret = new ArrayList<UserBranchPosition>();
		try{
			while(rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				int branch_id = rs.getInt("branch_id");
				String branch_name = rs.getString("branch_name");
				int position_id = rs.getInt("position_id");
				String position_name = rs.getString("position_name");
				int is_stopped = rs.getInt("is_stopped");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserBranchPosition userList = new UserBranchPosition();
				userList.setId(id);
				userList.setLogin_id(login_id);
				userList.setName(name);
				userList.setBranch_id(branch_id);
				userList.setBranchName(branch_name);
				userList.setPosition_id(position_id);
				userList.setPositionName(position_name);
				userList.setIs_stopped(is_stopped);
				userList.setCreated_date(createdDate);

				ret.add(userList);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
