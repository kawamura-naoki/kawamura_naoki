package kawamura_naoki.dao;

import static kawamura_naoki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import kawamura_naoki.beans.Branch;
import kawamura_naoki.exception.SQLRuntimeException;

public class BranchDao {

	public  List<Branch> pullDown(Connection connection) {

		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM branches ";

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Branch> ret = toBranchList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private static List<Branch> toBranchList(ResultSet rs)
			throws SQLException {

		List<Branch> ret = new ArrayList<Branch>();
		try{
			while(rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				Timestamp created_date = rs.getTimestamp("created_date");
				Timestamp updated_date = rs.getTimestamp("updated_date");

				Branch branch = new Branch();
				branch.setId(id);
				branch.setName(name);
				branch.setCreated_date(created_date);
				branch.setUpdated_date(updated_date);

				ret.add(branch);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
