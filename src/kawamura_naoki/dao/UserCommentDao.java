package kawamura_naoki.dao;

import static kawamura_naoki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import kawamura_naoki.beans.UserComment;
import kawamura_naoki.exception.SQLRuntimeException;

public class UserCommentDao {

	public static  List<UserComment> getUserComment(Connection connection) {

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.post_id as post_id, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("users.name as name, ");
			sql.append("comments.text as text, ");
			sql.append("comments.created_date as created_date ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ;");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toUserCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private static List<UserComment> toUserCommentList(ResultSet rs)
			throws SQLException {

		List<UserComment> ret = new ArrayList<UserComment>();
		try{
			while(rs.next()) {
				int post_id = rs.getInt("post_id");
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int user_id = rs.getInt("user_id");
				String text = rs.getString("text");
				Timestamp created_date = rs.getTimestamp("created_date");

				UserComment comment = new UserComment();
				comment.setPost_id(post_id);
				comment.setName(name);
				comment.setId(id);
				comment.setUser_id(user_id);
				comment.setText(text);
				comment.setCreated_date(created_date);

				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}

