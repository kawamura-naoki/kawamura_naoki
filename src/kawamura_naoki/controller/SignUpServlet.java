package kawamura_naoki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import kawamura_naoki.beans.Branch;
import kawamura_naoki.beans.Position;
import kawamura_naoki.beans.User;
import kawamura_naoki.service.BranchService;
import kawamura_naoki.service.PositionService;
import kawamura_naoki.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Position> positionList = new PositionService().toPositionList();
		List<Branch> branchList = new BranchService().toBranchList();
		request.setAttribute("positionList", positionList);
		request.setAttribute("branchList", branchList);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		List<Position> positionList = new PositionService().toPositionList();
		List<Branch> branchList = new BranchService().toBranchList();


		User user = new User();
		user.setLogin_id(request.getParameter("loginId"));
		user.setName(request.getParameter("name"));
		user.setPassword(request.getParameter("password"));

		String branchId = request.getParameter("branchId");
		String positionId = request.getParameter("positionId");



		if(isValid(request, messages) == true) {

			user.setBranch_id(Integer.parseInt(request.getParameter("branchId")));
			user.setPosition_id(Integer.parseInt(request.getParameter("positionId")));
			request.setAttribute("user",user);
			new UserService().register(user);
			response.sendRedirect("management");

		} else {

			request.setAttribute("user", user);
			request.setAttribute("branchId", branchId);
			request.setAttribute("positionId",positionId);
			request.setAttribute("errorMessages", messages);
			request.setAttribute("positionList", positionList);
			request.setAttribute("branchList", branchList);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("loginId");
		UserService userService = new UserService();
		User user = userService.confirmationUser(loginId);
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String confirmationPassword = request.getParameter("confirmationPassword");
		String branchId = request.getParameter("branchId");
		String positionId = request.getParameter("positionId");

		//エラーがあればメッセージに追加

		if(StringUtils.isBlank(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if(!loginId.matches("^[\\w]{6,20}+$")){
			messages.add("ログインIDは半角英数字6文字以上20文字以下です");
		}

		if(StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		} else if(name.length() > 10 ){
			messages.add("名前は10文字以下で入力してください");
		}

		if(StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		}else if(!password.matches("^[!\"#$%&'()*+,-./:;<=>?@\\[\\]^_`\\w]{6,20}?$")){
			messages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以下で入力してください");
		}else if(!password.equals(confirmationPassword)){
			messages.add("パスワードと確認パスワードが一致しません");
		}
		if(user != null){
			messages.add("ユーザーが重複しています");
		}
		if(StringUtils.isBlank(branchId) == true) {
			messages.add("支店を選択してください");
		}

		if(StringUtils.isBlank(positionId) == true) {
			messages.add("役職を選択してください");
		}
		//メッセージがなければtrue,あればfalseを返す
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
	}
}

