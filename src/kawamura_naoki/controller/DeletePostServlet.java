package kawamura_naoki.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kawamura_naoki.service.PostService;

@WebServlet(urlPatterns = { "/deletepost" })
public class DeletePostServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		int post_id = Integer.parseInt(request.getParameter("post_id"));

		PostService postService = new PostService();
		postService.delete(post_id);

		response.sendRedirect("./");
	}

}
