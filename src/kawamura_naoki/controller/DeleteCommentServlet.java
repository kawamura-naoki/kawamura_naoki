package kawamura_naoki.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kawamura_naoki.service.CommentService;

@WebServlet(urlPatterns = { "/deletecomment" })
public class DeleteCommentServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		int comment_id = Integer.parseInt(request.getParameter("comment_id"));

		CommentService commentService = new CommentService();
		commentService.delete(comment_id);

		response.sendRedirect("./");
	}

}