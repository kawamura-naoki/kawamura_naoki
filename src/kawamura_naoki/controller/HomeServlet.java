package kawamura_naoki.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kawamura_naoki.beans.User;
import kawamura_naoki.beans.UserComment;
import kawamura_naoki.beans.UserPost;
import kawamura_naoki.service.CommentService;
import kawamura_naoki.service.PostService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {


		HttpSession session = request.getSession();

		String defaultBigin = "2010-01-01 00:00;00";
		String begin_date = null;
		String end_date = null;
		request.setCharacterEncoding("UTF-8");
		String category = request.getParameter("category");
		String beginDate = request.getParameter("begin_date");
		String endDate = request.getParameter("end_date");


			if(beginDate == null || beginDate.length() == 0){
				begin_date = defaultBigin;
			} else {
				begin_date =  beginDate + " 00:00:00";
			}
			if(endDate == null || endDate.length() == 0){
				Date getDate = new Date();
				end_date =  new SimpleDateFormat("yyyy-MM-dd 23:59:59").format(getDate);
			}else{
				end_date =  endDate + " 23:59:59";
			}

			List<UserPost> posts = new PostService().getPost(begin_date, end_date, category);
			List<UserComment> comments = new CommentService().getComment();
			User user = (User) session.getAttribute("loginUser");

		request.setAttribute("user", user);
		request.setAttribute("posts", posts);
		request.setAttribute("comments", comments);
		request.setAttribute("begin_date",beginDate);
		request.setAttribute("end_date", endDate);
		request.setAttribute("category", category);
		request.getRequestDispatcher("/home.jsp").forward(request, response);

	}


	@Override
	protected void  doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {


	}
}

