package kawamura_naoki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import kawamura_naoki.beans.Comment;
import kawamura_naoki.beans.User;
import kawamura_naoki.service.CommentService;

@WebServlet(urlPatterns = { "/newcomment" })
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{

		HttpSession session = request.getSession();
		Comment comment = new Comment();

		List<String> messages = new ArrayList<String>();

		if(isValid(request,messages) == true){

			User user = (User) session.getAttribute("loginUser");

//			Comment comment = new Comment();
			comment.setPost_id(Integer.parseInt(request.getParameter("post_id")));
			comment.setUser_id(user.getId());
			comment.setText(request.getParameter("text"));

			new CommentService().register(comment);

			response.sendRedirect("./");
		} else {
			request.setAttribute("comment",comment);
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String text = request.getParameter("text");

		if(StringUtils.isBlank(text) == true){
			messages.add("コメントを入力してください");
		}else if(text.length() > 500){
			messages.add("500文字以下で入力してください");
		}
		if(messages.size() == 0){
			return true;
		} else {
			return false;
		}
	}
}
