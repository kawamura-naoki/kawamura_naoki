package kawamura_naoki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import kawamura_naoki.beans.Post;
import kawamura_naoki.beans.User;
import kawamura_naoki.service.PostService;

@WebServlet(urlPatterns = { "/newpost" })
public class NewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("newpost.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		User user = (User) session.getAttribute("loginUser");


		Post post = new Post();
		post.setTitle(request.getParameter("title"));
		post.setText(request.getParameter("post"));
		post.setUser_id(user.getId());
		post.setCategory(request.getParameter("category"));

		request.setAttribute("post",post);

		if(isValid(request, messages) == true){
			new PostService().register(post);
			response.sendRedirect("./");
		} else {
			request.setAttribute("errorMessages", messages);
			request.setAttribute("post", post);
			request.getRequestDispatcher("newpost.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String title = request.getParameter("title");
		String post = request.getParameter("post");
		String category = request.getParameter("category");

		if(StringUtils.isBlank(title) == true) {
			messages.add("タイトルを入力してください");
		} else if(title.length() > 30){
			messages.add("タイトルは30文字以下で入力してください");
		}
		if(StringUtils.isBlank(post) == true) {
			messages.add("本文を入力してください");
		} else if(post.length() > 1000){
			messages.add("本文は1000文字以下で入力してください");
		}
		if(StringUtils.isBlank(category) == true) {
			messages.add("カテゴリーを入力してください");
		} else if(category.length() > 10){
			messages.add("カテゴリーは10文字以下で入力してください");
		}
		if(messages.size() == 0){
			return true;
		} else {
			return false;
		}
	}
}

