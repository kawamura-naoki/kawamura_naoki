package kawamura_naoki.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import kawamura_naoki.beans.User;
import kawamura_naoki.beans.UserBranchPosition;
import kawamura_naoki.service.UserBranchPostService;
import kawamura_naoki.service.UserService;
@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<UserBranchPosition> userList = new UserBranchPostService().getUserList();
		request.setAttribute("userList", userList);
		request.getRequestDispatcher("/management.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {


		 int id = Integer.parseInt(request.getParameter("id"));
		 int is_stopped = Integer.parseInt(request.getParameter("is_stopped"));

		UserService userService = new UserService();
		if(is_stopped == 0){
			userService.isStop(id);
			response.sendRedirect("management");

		} else {
			userService.isResurrection(id);
			response.sendRedirect("management");
		}
	}
}