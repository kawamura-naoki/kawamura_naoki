package kawamura_naoki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import kawamura_naoki.beans.Branch;
import kawamura_naoki.beans.Position;
import kawamura_naoki.beans.User;
import kawamura_naoki.exception.NoRowsUpdatedRuntimeException;
import kawamura_naoki.service.BranchService;
import kawamura_naoki.service.PositionService;
import kawamura_naoki.service.UserService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		if(request.getParameter("id") == null || StringUtils.isBlank(request.getParameter("id")) || request.getParameter("id").matches("[\\D]+?")){
			messages.add("不正なパラメーターです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");

			return;
		}

		User nowEditUser =new UserService(). getUser(Integer.parseInt(request.getParameter("id")));


		if(nowEditUser == null){
			messages.add("ユーザーが存在しません");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}

		List<Position> positionList = new PositionService().toPositionList();
		List<Branch> branchList = new BranchService().toBranchList();

		request.setAttribute("positionList", positionList);
		request.setAttribute("branchList", branchList);
		request.setAttribute("nowEditUser",nowEditUser);

		request.getRequestDispatcher("/edit.jsp").forward(request,response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);
		List<Position> positionList = new PositionService().toPositionList();
		List<Branch> branchList = new BranchService().toBranchList();

		if(isValid(request, messages) == true){

			try{
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示します。");
				session.setAttribute("errorMessages", messages);
				request.getRequestDispatcher("edit.jsp").forward(request, response);
				return;
			}

			response.sendRedirect("management");

		} else {


			session.setAttribute("nowEditUser", editUser);
			request.setAttribute("errorMessages", messages);
			request.setAttribute("positionList", positionList);
			request.setAttribute("branchList", branchList);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLogin_id(request.getParameter("login_id"));
		editUser.setName(request.getParameter("name"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setBranch_id(Integer.parseInt(request.getParameter("branchId")));
		editUser.setPosition_id(Integer.parseInt(request.getParameter("positionId")));
		return editUser;
	}


	private boolean isValid(HttpServletRequest request, List<String> messages){

		int id = Integer.parseInt(request.getParameter("id"));
		String loginId = request.getParameter("login_id");
		UserService userService = new UserService();
		User user = userService.confirmationUser(loginId);
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String confirmationPassword = request.getParameter("confirmationPassword");

		//エラーがあればメッセージに追加



		if(StringUtils.isBlank(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if(!loginId.matches("^[\\w]{6,20}+$")){
			messages.add("ログインIDは半角英数字6文字以上20文字以下です");
		}

		if(StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		} else if(name.length() > 10 ){
			messages.add("名前は10文字以下で入力してください");
		}
		if(password.length() != 0){
			if(!password.matches("^[!\"#$%&'()*+,-./:;<=>?@\\[\\]^_`\\w]{6,20}?$")){
				messages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以下で入力してください");
			}else if(!password.equals(confirmationPassword)){
				messages.add("パスワードと確認パスワードが一致しません");
			}
		}
		if(user != null){
			if(user.getId() != id){
					messages.add("ユーザーが重複しています");
			}
		}
		//メッセージがなければtrue,あればfalseを返す
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
	}
}
