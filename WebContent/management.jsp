<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理画面</title>
	</head>
	<link href="css/bone.css" rel="stylesheet" type="text/css">
	<link href="css/management.css" rel="stylesheet" type="text/css">
	<script type="text/javascript">
		<!--
		function stop(){
			if(window.confirm('停止してよろしいですか？')){ // 確認ダイアログを表示
				return true; // 「OK」時は送信を実行
			}
			else{ // 「キャンセル」時の処理
				window.alert('キャンセルされました'); // 警告ダイアログを表示
				return false; // 送信を中止
			}
		}
		// -->
		<!--
		function resurrection(){
			if(window.confirm('復活してよろしいですか？')){ // 確認ダイアログを表示
				return true; // 「OK」時は送信を実行
			}
			else{ // 「キャンセル」時の処理
				window.alert('キャンセルされました'); // 警告ダイアログを表示
				return false; // 送信を中止
			}
		}
		// -->
	</script>
	<script language="JavaScript">
	<!--
	function myTimeprev(){
		var Nowymdhms = new Date();
		var NowYear = Nowymdhms.getYear() + 1900;
		var NowMon = Nowymdhms.getMonth() + 1;
		var NowDay = Nowymdhms.getDate();
		var NowWeek = Nowymdhms.getDay();
		var NowHour = Nowymdhms.getHours();
		var NowMin = Nowymdhms.getMinutes();
		var NowSec = Nowymdhms.getSeconds();
		myMsg = NowYear+"年"+NowMon+"月"+NowDay+"日<br>"+NowHour+"時"+NowMin+"分"+NowSec+"秒";
		document.getElementById("RealtimeClockArea").innerHTML = myMsg;
	}
	// -->
	</script>
	<body>
		<div class="top">
			業務連絡掲示板
		</div>
		<div class="sideMenu">
			<div class="name">
				<label><c:out value="${loginUser.name }" />さん<br/></label>
				<label>こんにちわ</label>
			</div>
			<div class="time">
			<script language="JavaScript">
			<!--
				setInterval("myTimeprev()",1000);
			 -->
			</script>
			<label>現在時刻：</label>
			<div class="time" name="RealtimeClockArea" id="RealtimeClockArea">*******</div>
			</div>
			<div class="header">
				<a href="signup">新規登録</a>
				<a href="./">ホーム</a>
			</div>
		</div>
		<div class="main-contents">
			<div class="title">
			<span class="under">ユーザー管理</span>
			</div>
			<c:if test="${ not empty errorMessages }" >
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="messages">
							<li><c:out value="${messages}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>
			<div class="userList">
				<table border="3" >
				<tr height="5">
					<th width="150" align="center" >名前</th><th width="100">ログインID</th><th width="150">所属支店</th><th width="100">役職</th><th width="100">停止・復活</th><th width="100">編集</th></tr>
					<c:forEach items="${userList}" var="user">
					<tr height="0">
					<td align="center"><span class="name"> <c:out value="${user.name }" /></span></td>
					<td align="center"><span class="login_id"><c:out value="${user.login_id }" /></span></td>
					<td align="center"><span class="branch_name"><c:out value="${user.branchName }"/></span></td>
					<td align="center"><span class="position_name"><c:out value="${user.positionName }"/></span></td>
					<td align="center" valign="middle">
					<c:if test="${loginUser.id != user.id}">
						<c:if test="${user.is_stopped == 0 }">
							<form action="management" method="post" onSubmit="return stop()">
								<input name="id" value="${user.id }" id="id" type="hidden"/>
								<input name="is_stopped" value="${user.is_stopped}" id="is_stopped" type="hidden"/>
								<input type="submit" value="停止" />
							</form>
						</c:if>
						<c:if test="${user.is_stopped == 1}">
							<form action="management" method="post" onSubmit="return resurrection()">
								<input name="id" value="${user.id }" id="id" type="hidden"/>
								<input name="is_stopped" value="${user.is_stopped}" id="is_stopped" type="hidden"/>
								<input type="submit" value="復活" />
							</form>
						</c:if>
					</c:if>
					</td>
					<td align="center">
						<form action="edit" method="get">
							<input name="id" value="${user.id }" id="id" type="hidden"/>
							<input type="submit" value="編集" />
						</form>
					</td>
					</tr>
					</c:forEach>
				</table>
			</div>
		</div>
</body>
</html>