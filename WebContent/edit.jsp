<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー編集画面</title>
		<link href="css/bone.css" rel="stylesheet" type="text/css">
		<link href="css/sginUpEdit.css" rel="stylesheet" type="text/css">
	</head>
		<script language="JavaScript">
	<!--
	function myTimeprev(){
		var Nowymdhms = new Date();
		var NowYear = Nowymdhms.getYear() + 1900;
		var NowMon = Nowymdhms.getMonth() + 1;
		var NowDay = Nowymdhms.getDate();
		var NowWeek = Nowymdhms.getDay();
		var NowHour = Nowymdhms.getHours();
		var NowMin = Nowymdhms.getMinutes();
		var NowSec = Nowymdhms.getSeconds();
		myMsg = NowYear+"年"+NowMon+"月"+NowDay+"日<br>"+NowHour+"時"+NowMin+"分"+NowSec+"秒";
		document.getElementById("RealtimeClockArea").innerHTML = myMsg;
	}
	// -->
	</script>
	<body>
		<div class="top">
			業務連絡掲示板
		</div>
		<div class="sideMenu">
			<div class="name">
				<label><c:out value="${loginUser.name }" />さん<br/></label>
				<label>こんにちわ</label>
			</div>
			<div class="time">
			<script language="JavaScript">
			<!--
				setInterval("myTimeprev()",1000);
				 -->
			</script>
				<label>現在時刻：</label>
				<div class="time" name="RealtimeClockArea" id="RealtimeClockArea">*******</div>
			</div>
			<a href="management">ユーザー管理</a>
		</div>
		<div class="main-contents">
			<div class="title">
				<span class="under">ユーザー編集</span>
			</div>
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessage">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
			</c:if>
			<div class="edit">
				<form action="edit" method="post">
				<input name="id" value="${nowEditUser.id}" id="id" type="hidden">
					<table border="2" >
						<tr height="5">
								<th><label for="login_id">ログインID</label></th>
								<th><input name="login_id" value="${nowEditUser.login_id}" id="login_id"/></th></tr>
							<tr height="5">
								<th><label for="name">名前</label></th>
								<th><input name="name" value="${nowEditUser.name}"  id="name"/></th></tr>
							<tr height="5">
								<th><label for="password">パスワード</label></th>
								<th><input type="password" name="password" id="password"/></th></tr>
							<tr height="5">
								<th><label for="confirmationPassword">確認パスワード</label></th>
								<th><input type="password" name="confirmationPassword"  id="confirmationPassword" /></th></tr>
								<c:if test="${loginUser.id == nowEditUser.id}">
									<input name="branchId" value="${nowEditUser.branch_id}" id="branchId" type="hidden"/>
									<input name="positionId" value="${nowEditUser.position_id}" id="positionId" type="hidden"/>
								</c:if>
								<c:if test="${loginUser.id != nowEditUser.id}">
								<tr height="5">
									<th><label for="branch_id">支店コード</label></th>
									<th><select name="branchId">
										<c:forEach items="${branchList}" var="branch">
											<c:if test="${nowEditUser.branch_id == branch.id}">
												<option value="${branch.id}" selected >${branch.name}</option>
											</c:if>
											<c:if test="${nowEditUser.branch_id != branch.id}">
												<option value="${branch.id}" >${branch.name}</option>
											</c:if>
										</c:forEach>
									</select>
									</th>
								</tr>
								<tr height="5">
									<th><label for="position_id">役職コード</label></th>
									<th><select name="positionId">
										<c:forEach items="${positionList}" var="position">
											<c:if test="${nowEditUser.position_id == position.id}">
												<option value="${position.id}" selected >${position.name}</option>
											</c:if>
											<c:if test="${nowEditUser.position_id != position.id}">
												<option value="${position.id}" >${position.name}</option>
											</c:if>
										</c:forEach>
									</select>
									</th>
								</tr>
							</c:if>
						</table>
					<input type="submit" value="登録" id="submit">
				</form>
			</div>
			<c:if test="${loginUser.id != nowEditUser.id}">
			<div class="note1">
				半角英数字6文字以上20文字以下<br/>
				10文字以内<br/>
				記号を含む半角英数字6文字以上20文字以下<br/>
			</div>
			</c:if>
			<c:if test="${loginUser.id == nowEditUser.id}">
			<div class="note2">
				半角英数字6文字以上20文字以下<br/>
				10文字以内<br/>
				記号を含む半角英数字6文字以上20文字以下<br/>
			</div>
			</c:if>
		<c:remove var="nowEditUser" scope="session"/>
		</div>
	</body>
</html>