<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Home page</title>
	</head>
	<link href="css/bone.css" rel="stylesheet" type="text/css">
	<link href="css/home.css" rel="stylesheet" type="text/css">
	<script type="text/javascript">
		<!--
		function check(){
			if(window.confirm('削除してよろしいですか？')){ // 確認ダイアログを表示
				return true; // 「OK」時は送信を実行
			}
			else{ // 「キャンセル」時の処理
				window.alert('キャンセルされました'); // 警告ダイアログを表示
				return false; // 送信を中止
			}
		}
		// -->
	<!--
	function ClearButton_Click()
{
	document.getElementById("begin_date").value = "";
	document.getElementById("end_date").value = "";
	document.getElementById("category").value = "";
}
	// -->
	<!--
	function myTimeprev(){
		var Nowymdhms = new Date();
		var NowYear = Nowymdhms.getYear() + 1900;
		var NowMon = Nowymdhms.getMonth() + 1;
		var NowDay = Nowymdhms.getDate();
		var NowWeek = Nowymdhms.getDay();
		var NowHour = Nowymdhms.getHours();
		var NowMin = Nowymdhms.getMinutes();
		var NowSec = Nowymdhms.getSeconds();
		myMsg = NowYear+"年"+NowMon+"月"+NowDay+"日<br>"+NowHour+"時"+NowMin+"分"+NowSec+"秒";
		document.getElementById("RealtimeClockArea").innerHTML = myMsg;
	}
	// -->
	</script>
	<body>
		<div class="top">
			業務連絡掲示板
		</div>
		<div class="sideMenu">
			<div class="name">
				<label><c:out value="${loginUser.name }" />さん<br/></label>
				<label>こんにちわ</label>
			</div>
			<div class="time">
			<script language="JavaScript">
			<!--
				setInterval("myTimeprev()",1000);
				 -->
			</script>
				<label>現在時刻：</label>
				<div class="time" name="RealtimeClockArea" id="RealtimeClockArea">*******</div>
			</div>
			<a href="newpost">新規投稿</a><br/>
			<c:if test="${user.branch_id == 1 && user.position_id == 1 }">
				<a href="management">ユーザー管理</a><br/>
			</c:if>
			<a href="logout">ログアウト</a>
		</div>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }" >
				<div class="errorMessage">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				<c:remove var="errorMessages" scope="session"/>
				</div>
			</c:if>

			<div class="retrieval">
			<label>日付カテゴリ検索</label>
				<form  method="get">
					<div class="date">
					<label>日付</label><br/>
						<input type="date" name="begin_date" value="${begin_date}" id="begin_date"  style="font-weght:bold">
					<label>～</label>
						<input type="date" name="end_date" value="${end_date}" id="end_date" style="font-weght:bold"><br/>
					</div>
					<div class="category">
					<label>カテゴリ</label><br/>
						<input  name="category" id="category" value="${category}" >
					<input type="submit" value="検索">
					<input type="Button" value="クリア" onclick="ClearButton_Click()">
					</div>
				</form>
			</div>
			<c:forEach items="${posts}" var="post">
				<div class="posts">
					<div class="title"><span class="p"><c:out value="${post.title}" /></span></div>
					<div class="name"><c:out value="${post.name}" /></div>
					<div class="text">
					<c:forEach var="text" items="${fn:split(post.text,'
')}" ><br/><c:out value="${text}" />
					</c:forEach>
					</div>
					<div class="other">
						<c:out value="${post.category}"/>
			 			<fmt:formatDate value="${post.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />
			 		</div>
			 		<div class="submit">
						<input name="post_id" value="${post.user_id }" type="hidden">
						<c:if test="${user.id == post.user_id }" >
							<form action="deletepost" method="post"onSubmit="return check()">
								<input name="post_id" value="${post.id }" id="post_id" type="hidden">
								<input type="submit" value="投稿削除">
							</form>
						</c:if><br />
					</div>
				</div>
				<div class="comment">
					<div class="title">
						<label>コメント</label>
					</div>
					<div class="text">
					<c:forEach items="${comments}" var="comment">
							<c:if test="${post.id  == comment.post_id}" >
								<c:forEach var="text" items="${fn:split(comment.text,'
')}" ><br/><c:out value="${text}" />
								</c:forEach>
								<br/>
						 		<c:out value="${comment.name}" />
						 		<fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />
					 			<c:if test="${user.id == comment.user_id }" >
									<form action="deletecomment" method="post" onSubmit="return check()">
										<input name="comment_id" value="${comment.id }" id="comment_id" type="hidden">
										<input type="submit" value="コメント削除">
									</form>
								</c:if><br />
						 	</c:if>
						</c:forEach>
					</div>
				</div>
				<div class="newcomment">
					<form action="newcomment" method="post">
						<input name="post_id" value="${post.id }" id="post_id" type="hidden">
						<textarea name="text" cols="50" rows="2" class="text">${comment.text }</textarea>500文字以内<br />
						<input type="submit" value="コメント">
					</form>
				</div>
			</c:forEach>
		</div>
	</body>
</html>