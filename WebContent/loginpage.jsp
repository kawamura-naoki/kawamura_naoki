<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Login Page</title>
		<link href="./css/login.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="title">
			業務連絡掲示板
		</div>
		<div class="login_box">
			<div class="errorMessages">
				<c:if test="${ not empty errorMessages }" >
					<c:forEach items="${errorMessages}" var="message">
					<br/>
						<span><c:out value="${message}" /></span>
					</c:forEach>
				</c:if>
			</div>
			<c:remove var="errorMessages" scope="session"/>

			<div class="login">
				<form action="login" method="post"><br />
					<table>
						<tr>
							<td><label for="login_id" >ログインID</label></td>
							<td>
							<input name="login_id" value="${login_id}" id="login_id"/></td>
						</tr>
						<tr>
							<td><label for="password">パスワード</label></td>
							<td>
							<input type="password" name="password"  id="password" /></td>
						</tr>
					</table>
					<div class="button">
							<input type="submit" value="ログイン" >
					</div>
				</form>
			</div>
		 </div>
	</body>
</html>