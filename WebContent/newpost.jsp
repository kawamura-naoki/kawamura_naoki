<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿</title>
	</head>
	<link href="css/bone.css" rel="stylesheet" type="text/css">
	<link href="css/newPost.css" rel="stylesheet" type="text/css">
	<script language="JavaScript">
	<!--
	function myTimeprev(){
		var Nowymdhms = new Date();
		var NowYear = Nowymdhms.getYear() + 1900;
		var NowMon = Nowymdhms.getMonth() + 1;
		var NowDay = Nowymdhms.getDate();
		var NowWeek = Nowymdhms.getDay();
		var NowHour = Nowymdhms.getHours();
		var NowMin = Nowymdhms.getMinutes();
		var NowSec = Nowymdhms.getSeconds();
		myMsg = NowYear+"年"+NowMon+"月"+NowDay+"日<br>"+NowHour+"時"+NowMin+"分"+NowSec+"秒";
		document.getElementById("RealtimeClockArea").innerHTML = myMsg;
	}
	// -->
	</script>
	<body>
		<div class="top">
			<label>業務連絡掲示板</label>
		</div>
		<div class="sideMenu">
			<div class="name">
				<label><c:out value="${loginUser.name }" />さん<br/></label>
				<label>こんにちわ</label>
			</div>
			<div class="time">
			<script language="JavaScript">
			<!--
				setInterval("myTimeprev()",1000);
				 -->
			</script>
				<label>現在時刻：</label>
				<div class="time" name="RealtimeClockArea" id="RealtimeClockArea">*******</div>
			</div>
			<a href="./">ホーム</a>
		</div>


		<div class="main-contents">
		<div class="title">
			<label><span class="under">新規投稿</span></label>
		</div>

			<c:if test="${not empty errorMessages }" >
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="messages">
							<li><c:out value="${messages}" />
						</c:forEach>
					</ul>
				</div>
			</c:if>

			<div class="post">
				<form action="newpost" method="post"><br />
					<table  border="2" >
						<tr>
							<td><label for="title">タイトル</label></td>
							<td><input name="title"  value="${post.title}" id="title" style="width: 544px;"/></td></tr>
						<tr>
							<td><label for="post">本文</label></td>
							<td><textarea  name="post" cols="75" rows="5" class="post-box" >${post.text}</textarea></td></tr>
						<tr>
							<td><label for="category">カテゴリー</label></td>
							<td><input name="category"  value="${post.category}" id="category" style="width: 544px;"/></td></tr>
					</table>
					<br/>
				<input type="submit" value="投稿" />
				</form>
				<div class="note">
					30文字以内<br />
					1000文字以内<br />
					10文字以内<br />
				</div>
			</div>
		</div>
	</body>
</html>